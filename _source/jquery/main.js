$(document).ready(function() {

  $('.slider').slick({
    dots: false,
    arrows: false,
    speed: 1500,
    autoplay: true,
    autoplaySpeed: 15000,
    pauseOnHover: false,
  });

  $('.tooltip-bottom').tooltipster({
    side: 'bottom',
    theme: ['tooltipster-black']
  });
});
