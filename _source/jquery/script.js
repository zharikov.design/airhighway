
$(document).ready(function() {

  $('header').scrollToFixed();

  $('html.mobile body, html.tablet body').on('click', '.burger', function(){
    $(this).toggleClass('active');
    $('header div nav, header div div').toggleClass('active');
  });

  $('html.mobile body, html.tablet body').on('click', 'header div ul li a', function(){
    $(this).next('ul').toggleClass('active');
  });

});
