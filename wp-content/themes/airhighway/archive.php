<?php get_header(); ?>

<div class="page-page container">
	<?php if ( have_posts() ) : ?>
	<?php the_archive_title( '<h1>', '</h1>' ); ?>
	<div class="feed">
		<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'pages/archive', 'excerpt' );
			endwhile;

			else :
				get_template_part( 'pages/none', 'none' );
			endif;
		?>
  </div>
</div>

<?php get_footer(); ?>