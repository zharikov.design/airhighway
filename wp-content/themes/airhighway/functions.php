<?php

// Title
add_theme_support( 'title-tag' );

// Сброс классов body
add_filter('body_class','my_class_names');
function my_class_names($classes) {
  return array();
}

// Удаляем "Рубрика: ", "Метка: " и т.д. из заголовка архива
add_filter( 'get_the_archive_title', function( $title ){
	return preg_replace('~^[^:]+: ~', '', $title );
});

// Удаление конструкции [...] на конце
add_filter('excerpt_more', function($more) {
	return '...';
});

register_nav_menus(array(
	'header'    => 'Верхнее меню', // Название месторасположения меню в шаблоне
	'footer' => 'Нижнее меню'      // Название другого месторасположения меню в шаблоне
));

// Шаблоны для страницы
add_filter('template_include', 'my_template');
function my_template( $template ) {

  // Главная
	if( is_page('glavnaya') ){
		if ( $new_template = locate_template( array( 'index.php' ) ) )
			return $new_template ;
  }

  if( is_page('main') ){
		if ( $new_template = locate_template( array( 'index_eng.php' ) ) )
			return $new_template ;
  }

  // Сервис
	if( is_page('service') ){
		if ( $new_template = locate_template( array( 'pages/service.php' ) ) )
			return $new_template ;
  }
  if( is_page('services') ){
		if ( $new_template = locate_template( array( 'pages/service.php' ) ) )
			return $new_template ;
  }

  // Галерея
	if( is_page('gallery') ){
		if ( $new_template = locate_template( array( 'pages/gallery.php' ) ) )
			return $new_template ;
  }

  // Партнеры
	if( is_page('partner') ){
		if ( $new_template = locate_template( array( 'pages/partners.php' ) ) )
			return $new_template ;
  }
  if( is_page('partners') ){
		if ( $new_template = locate_template( array( 'pages/partners.php' ) ) )
			return $new_template ;
  }

  // Контакты
	if( is_page('contact') ){
		if ( $new_template = locate_template( array( 'pages/contacts.php' ) ) )
			return $new_template ;
  }
  if( is_page('contacts') ){
		if ( $new_template = locate_template( array( 'pages/contacts.php' ) ) )
			return $new_template ;
  }

  // Новости
	if( is_page('news') ){
		if ( $new_template = locate_template( array( 'pages/news.php' ) ) )
			return $new_template ;
  }

  // Раскрасска
	if( is_page('rasskraska-kombenizonov') ){
		if ( $new_template = locate_template( array( 'pages/colorings.php' ) ) )
			return $new_template ;
  }

  if( is_page('coloring-jumpsuits') ){
		if ( $new_template = locate_template( array( 'pages/colorings.php' ) ) )
			return $new_template ;
  }

  // Политика конфиденциальности
	if( is_page('privacy-policy') ){
		if ( $new_template = locate_template( array( 'pages/blank.php' ) ) )
			return $new_template ;
  }
  if( is_page('privacy-policy-eng') ){
		if ( $new_template = locate_template( array( 'pages/blank.php' ) ) )
			return $new_template ;
  }

  // Пользовательское соглашение
	if( is_page('terms-policy') ){
		if ( $new_template = locate_template( array( 'pages/blank.php' ) ) )
			return $new_template ;
  }
  if( is_page('terms-policy-eng') ){
		if ( $new_template = locate_template( array( 'pages/blank.php' ) ) )
			return $new_template ;
  }

	return $template;
}

// Шаблоны категорий
add_filter('single_template', create_function(
  '$the_template',
  'foreach( (array) get_the_category() as $cat ) {
    if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
    return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
  return $the_template;' )
);

// Миниатюра поста
add_theme_support('post-thumbnails');

// Cброс Блок стилей галереи
function custom_theme_assets() {
	wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'custom_theme_assets', 100 );

// Виджет в подвале
if ( function_exists('register_sidebar') )
  register_sidebar(array(
      'name' => 'Контакты',
      'description' => 'Вывод контактной информации в подвале сайта.',
      'before_widget' => '<div class="newsidebar">',
      'after_widget' => '</div>',
      'before_title' => '<div class="title">',
      'after_title' => '</div>',
  )
);

// Виджет для меню продукция
if ( function_exists('register_sidebar') )
  register_sidebar(array(
      'name' => 'Продукция',
      'description' => 'Вывод названия меню.',
      'before_widget' => '<div class="newsidebar">',
      'after_widget' => '</div>',
      'before_title' => '<div class="title">',
      'after_title' => '</div>',
  )
);

// Виджет для вывода базовой стоимости
if ( function_exists('register_sidebar') )
  register_sidebar(array(
      'name' => 'Базовая стоимость',
      'description' => 'Вывод текста «Базоая стоимость»',
      'before_widget' => '<div class="newsidebar">',
      'after_widget' => '</div>',
      'before_title' => '<div class="title">',
      'after_title' => '</div>',
  )
);

?>