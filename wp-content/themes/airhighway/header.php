<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="yandex-tableau-widget" content="logo=<?php echo get_template_directory_uri() ?>/images/ya-bro.png, color=#0a6da6" />
  <meta content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0' name='viewport'>
  <meta content='<?php echo get_post_meta($post->ID, 'description', true); ?>' name='description'>
  <meta content='<?php echo get_post_meta($post->ID, 'keywords', true); ?>' name='keywords'>
  <meta content='True' name='HandheldFriendly'>
  <meta content='320' name='MobileOptimized'>
  <meta content='Russian' name='language'>
	<meta content='3' name='revisit-after'>
	
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_template_directory_uri() ?>/images/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/images/favicon/safari-pinned-tab.svg" color="#333333">
  <meta name="apple-mobile-web-app-title" content="AirHighway">
  <meta name="application-name" content="AirHighway">
  <meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	
	<link href='<?php echo get_template_directory_uri() ?>/style.css' media='screen, projection' rel='stylesheet' type='text/css'>
  <script src='<?php echo get_template_directory_uri() ?>/js/page/script.min.js' type='text/javascript'></script>
  <script src='<?php echo get_template_directory_uri() ?>/js/plugin/device.min.js' type='text/javascript'></script>

	<?php wp_head(); ?>
</head>
<script src="https://yastatic.net/browser-updater/v1/script.js" charset="utf-8"></script><script>var yaBrowserUpdater = new ya.browserUpdater.init({"lang":"ru","browsers":{"yabrowser":"16.12","chrome":"62","ie":"10","opera":"49","safari":"9.1","fx":"57","iron":"35","flock":"Infinity","palemoon":"25","camino":"Infinity","maxthon":"4.5","seamonkey":"2.3"},"theme":"blue"});</script>

<body <?php body_class(); ?>>
<header>
  <div class="container">
    <a href="<?php echo get_home_url() ?>" class="logo"></a>
    <?php wp_nav_menu(array('menu' => 'header-rus', 'menu_class' => '', 'menu_class' => '')); ?>
    <ul class="lang"><?php pll_the_languages(); ?></ul>
    <a href="colorMain.html" class="palitra">
      <span class="icon"></span>
    </a>
    <a href="javascript:void(0);" class="burger"></a>
  </div>
</header>