<?php
  get_header();
  wp_enqueue_script('newscript', get_template_directory_uri() . '/js/page/main.min.js');
?>

<div class="page-main">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php the_content(); ?>
  <?php endwhile; else: ?>
  <?php endif; ?>

  <div class="news">
    <h2><span>Новости</span></h2>
    <div class="container">
    <?php	query_posts('category_name=news'); while (have_posts()) : the_post();?>
      <a href="<?php the_permalink(); ?>" class="news-item">
        <div class="news-item-img" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
        <div class="news-item-title"><?php the_title(); ?></div>
        <div class="news-item-date"><?php the_time('j F Y') ?></div>
        <div class="news-item-text"><?php the_excerpt(); ?></div>
      </a>
      <?php 
      endwhile;
      wp_reset_query();
    ?>
    </div>
    <div class="center">
      <a href="/news" class="button_border _gray _h-blue"><span>Все новости</span></a>
    </div>
  </div>
</div>

<?php
	get_footer();
?>
