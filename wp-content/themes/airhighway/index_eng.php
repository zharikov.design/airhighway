<?php
  get_header();
  wp_enqueue_script('newscript', get_template_directory_uri() . '/js/page/main.min.js');
?>

<div class="page-main">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php the_content(); ?>
  <?php endwhile; else: ?>
  <?php endif; ?>
</div>

<?php
	get_footer();
?>
