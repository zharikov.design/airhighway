<div <?php post_class('page-class'); ?> data-href="<?php the_permalink(); ?>">
  <!-- Images post URL -->
  <?php echo get_the_post_thumbnail_url(); ?> 

  <!-- Images post URL -->
  <?php echo get_the_post_thumbnail(); ?>

  <!-- Category -->
  <?php the_category(); ?>

  <!-- Post link -->
  <a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a>

  <!-- Post date -->
  <?php the_date(); ?>

  <!-- Comments -->
  <?php comments_popup_link( '0', '1', '%', '', '—'); ?>
</div>