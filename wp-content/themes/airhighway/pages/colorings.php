<?php 
//Template Name: Расскарска
get_header(); 
wp_enqueue_script('newscript', get_template_directory_uri() . '/js/page/produkt.min.js');
?>

<div class="page-produkt">
  <nav class="sub lang-rus">
    <div class="container">
      <h1>Продукция</h1>
      <?php wp_nav_menu(array('menu' => 'produkt-rus')); ?>
    </div>
  </nav>
  <nav class="sub lang-eng">
    <div class="container">
      <h1>Product</h1>
      <?php wp_nav_menu(array('menu' => 'produkt-eng')); ?>
    </div>
  </nav>
  <div class="container main">
    <div class="main-produkt">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; else: ?>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>