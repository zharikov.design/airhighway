<?php get_header(); ?>
  <div class="page-news main">
    <h1><?php the_title(); ?></h1>
    
    <div class="main-wrapp container">
    <?php	query_posts('category_name=news'); while (have_posts()) : the_post();?>

      <a href="<?php the_permalink(); ?>" class="main-wrapp_item">
        <div class="main-wrapp_item-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
        <div class="main-wrapp_item-text">
          <h2><?php the_title(); ?></h2>
          <span><?php the_time('j F Y') ?></span>
          <p><?php the_excerpt(); ?></p>
        </div>
      </a>
      
    <?php 
      endwhile;
      wp_reset_query();
    ?>
    </div>
    
  </div>
<?php get_footer(); ?>