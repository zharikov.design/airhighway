<?php get_header(); ?>
  <div class="page-partners">
    <h1><?php the_title(); ?></h1>
    <div class="container">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; else: ?>
      <?php endif; ?>
    </div>
  </div>
<?php get_footer(); ?>