<?php 
//Template Name: Комбинезон
get_header(); 
wp_enqueue_script('newscript', get_template_directory_uri() . '/js/page/produkt.min.js');
?>

<div class="page-produkt">
<nav class="sub">
  <div class="container">
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Продукция') ) : ?>
    <?php endif; ?>
    <?php wp_nav_menu(array('menu' => 'produkt-rus')); ?>
  </div>
</nav>
  <div class="page">
    <div class="page-title container">
      <h2><?php the_title(); ?></h2>
      <div class="page-title-price">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Базовая стоимость') ) : ?>
        <?php endif; ?>
        <span><?php the_field('suit_stoimost'); ?><span></span></span>
      </div>
    </div>
    <div class="page-images" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; else: ?>
    <?php endif; ?>
  </div>
</div>

<?php get_footer(); ?>