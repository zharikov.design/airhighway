<?php get_header(); ?>

<div class="page-search container">

	<h1>
		Вы искали:
		<span class="srh"><?php echo $_GET['s'];?></span>
		<span class="result">Мы нашли <?php global $wp_query; echo $wp_query->found_posts; ?> результатов</span>
	</h1>

	<div class="page">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class('item'); ?> data-href="<?php the_permalink();?>">
			<div class="item-head">
				<a href="<?php the_permalink();?>" class="item-head-title"><?php the_title(); ?></a>
				<?php the_category(); ?>
				<span class="item-head-class"><?php echo (get_post_meta($post->ID, 'class', true)) ?></span>
				<div class="item-head-date"><?php the_time('d F Y') ?></div>
			</div>
			<div class="item-content"><?php the_excerpt(); ?></div>
		</div>

		<?php endwhile; else: ?>
		<div class="item-noresult">Мы искали, но не нашли :( </div>
		<?php endif;?> 
	</div>
</div>

<?php
	wp_enqueue_script('newscript', get_template_directory_uri() . '/js/plugin/data-href.min.js');
	get_footer();
?>