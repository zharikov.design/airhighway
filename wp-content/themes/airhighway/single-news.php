<?php get_header(); ?>

  <div class="page-news page">
    <h1><?php the_title(); ?></h1>
    <div class="page-date"><?php the_time('j F Y') ?></div>
    <div class="container">
      <div class="page-img">
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img">
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="blur">
      </div>
      <div class="page-content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; else: ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php get_footer(); ?>